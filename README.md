# Stock Market Simulator Multiplater Game 

The basic idea of this project is to form a program that enables people to form and maintain a stock investments composed of shares from a virtual stock market without any financial dedication. Trading shares over a simulated stock market is mean to produce observe for a freshman that interested in stock market.
An investor is a player that can be either a buyer or a seller. Therefore, we assume that at the beginning of the game the investor is a buyer. The registered companies can sell shares which can be bought by a buyer. Shares can only be bought during a turn in a fixed time period. This means in every game there will be a fixed number of turns that lasting for a given period of time. A stock broker will act as the intermediary between a company and an investor. A Stock broker perform transactions like purchase or sale of stocks. Finally, at the end of the game the player with the majority of net value will win the game.

GitLab Repository  - https://gitlab.com/Minks6/comp3006l_se2_project
