package com.stock.market.simulator.simulator.Actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.BankTransaction;
import com.stock.market.simulator.simulator.Messages.CheckBankAccountBalance;
import com.stock.market.simulator.simulator.Messages.CreateBankAccount;
import com.stock.market.simulator.simulator.Messages.GetAllUsersRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BankActor extends AbstractActor{

    private Map<String,Float> bankLedger = new HashMap<>();

    public static Props props(){
        return Props.create(BankActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CreateBankAccount.class, msg->{
                    bankLedger.put(msg.getAccountHolderName(),msg.getAmount());
                })
                .match(CheckBankAccountBalance.class, msg->{
                    float balance = bankLedger.get(msg.getAccountName());
                    getSender().tell(balance,getSelf());
                })
                .match(GetAllUsersRequest.class, msg->{
                    List<UserDto> userList = new ArrayList<>();
                    this.bankLedger.forEach((name,amount)->{
                        UserDto userDto = new UserDto();
                        userDto.setUsername(name);
                        userDto.setAmount(amount);
                        userList.add(userDto);
                    });
                    getSender().tell(userList,getSelf());
                })
                .match(BankTransaction.class,bankTransaction -> {
                    if(bankTransaction.getAction().equals("withdraw")){
                        bankLedger.put(bankTransaction.getUsername(),(bankLedger.get(bankTransaction.getUsername())-bankTransaction.getAmount()));
                    }else if(bankTransaction.getAction().equals("deposit")){
                        bankLedger.put(bankTransaction.getUsername(),(bankLedger.get(bankTransaction.getUsername())+bankTransaction.getAmount()));
                    }
                })
                .build();
    }
}
