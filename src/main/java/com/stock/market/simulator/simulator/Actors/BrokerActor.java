package com.stock.market.simulator.simulator.Actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.stock.market.simulator.simulator.Controller.ActorController;
import com.stock.market.simulator.simulator.Controller.BankController;
import com.stock.market.simulator.simulator.Controller.BrokerController;
import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Messages.BankTransaction;
import com.stock.market.simulator.simulator.Messages.StockRequest;
import com.stock.market.simulator.simulator.Messages.Transaction;
import com.stock.market.simulator.simulator.Models.StockData;
import com.stock.market.simulator.simulator.Models.TransactionStatus;
import scala.concurrent.java8.FuturesConvertersImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BrokerActor extends AbstractActor {

    private Map<String,List<StockData>> portfolio = new HashMap<>();
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(Transaction.class,transaction -> {
                    List<StockData> stockData = portfolio.get(transaction.getUsername());
                    if(stockData == null){
                        stockData = new ArrayList<>();
                    }
                    TransactionStatus result = new TransactionStatus();
                    if(transaction.getAction().equals("buy")){
                        float totalNeeded = transaction.getStock().getPrice() * transaction.getAmount();
                        boolean available = BrokerController.checkBankAccount(transaction.getUsername(),totalNeeded);
                        if(available){

                            List<StockData> isStockAvailable = new ArrayList<>();
                            stockData.forEach(stock -> {
                                if(stock.getStock().getName().equals(transaction.getStock().getName())){
                                    stock.setAmount(transaction.getAmount()+stock.getAmount());
                                    isStockAvailable.add(stock);
                                }
                            });
                            if(isStockAvailable.size() == 0){
                                StockData stock = new StockData();
                                stock.setStock(transaction.getStock());
                                stock.setAmount(transaction.getAmount());
                                stockData.add(stock);
                            }

                            portfolio.put(transaction.getUsername(),stockData);
                            result.setStatus(true);
                            result.setStocks(stockData);
                            result.setMsg("Buy transaction successful");
                            BankTransaction bankTransaction = new BankTransaction();
                            bankTransaction.setAction("withdraw");
                            bankTransaction.setUsername(transaction.getUsername());
                            bankTransaction.setAmount(transaction.getStock().getPrice()*transaction.getAmount());
                            BankController.transaction(bankTransaction);
                        }else{
                            result.setMsg("No sufficient funds");
                            result.setStatus(false);
                            result.setStocks(stockData);
                        }

                    }else{
                        List<StockData> isStockAvailable = new ArrayList<>();
                        stockData.forEach(stock -> {
                            if(stock.getStock().getName().equals(transaction.getStock().getName())){
                                if(stock.getAmount() < transaction.getAmount()){
                                    result.setStatus(false);
                                    result.setMsg("Not enough stocks to sell !!");
                                }else{
                                    stock.setAmount(stock.getAmount()-transaction.getAmount());
                                    result.setStatus(true);
                                    result.setMsg("Sell transaction successful");
                                    BankTransaction bankTransaction = new BankTransaction();
                                    bankTransaction.setAction("deposit");
                                    bankTransaction.setUsername(transaction.getUsername());
                                    bankTransaction.setAmount(transaction.getStock().getPrice()*transaction.getAmount());
                                    BankController.transaction(bankTransaction);
                                }
                                isStockAvailable.add(stock);
                            }

                        });

                        if(isStockAvailable.size() == 0){
                            result.setMsg("Stock not available to sell !!");
                            result.setStatus(false);
                        }
                        result.setStocks(stockData);
                    }

                    getSender().tell(result,getSelf());
                })
                .match(StockRequest.class,msg->{
                    List<StockData> stockList = new ArrayList<>();
                    if(portfolio.get(msg.getUsername()) != null){
                        stockList = portfolio.get(msg.getUsername());
                    }
                   getSender().tell(stockList,getSelf());
                }).build();
    }

    public static Props props(){
        return Props.create(BrokerActor.class);
    }
}
