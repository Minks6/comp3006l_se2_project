package com.stock.market.simulator.simulator.Actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.stock.market.simulator.simulator.Messages.CurrentTurnRequest;
import com.stock.market.simulator.simulator.Messages.IncreaseTurnRequest;

public class ClockActor extends AbstractActor {
    private static int turn = 1;
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CurrentTurnRequest.class, msg->{
                    getSender().tell(turn,getSelf());
                })
                .match(IncreaseTurnRequest.class,msg->{
                    turn+=1;
                    getSender().tell(turn,getSelf());
                })
                .build();
    }
    public static Props props(){
        return Props.create(ClockActor.class);
    }
}
