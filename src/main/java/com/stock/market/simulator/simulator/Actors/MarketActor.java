package com.stock.market.simulator.simulator.Actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Messages.InitializeStocks;
import com.stock.market.simulator.simulator.Messages.UpdateStockPrices;

public class MarketActor extends AbstractActor {

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(InitializeStocks.class,msg->{
                    GameController.initializeStocks();
                })
                .match(UpdateStockPrices.class,msg->{
                    GameController.stocks.forEach(stock -> {
                        stock.calculateNewPrice(msg.getTurn());
                    });
                })
                .build();
    }

    public static Props props(){
        return Props.create(MarketActor.class);
    }
}
