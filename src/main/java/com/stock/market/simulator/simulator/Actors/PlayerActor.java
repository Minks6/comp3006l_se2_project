package com.stock.market.simulator.simulator.Actors;

import akka.actor.AbstractActor;
import akka.actor.Props;
import com.stock.market.simulator.simulator.Controller.BrokerController;
import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Messages.*;
import com.stock.market.simulator.simulator.Models.Stock;
import com.stock.market.simulator.simulator.Models.StockData;

import java.util.List;

public class PlayerActor extends AbstractActor {

    private int currentTurn = 0;

    public static Props props(){
        return Props.create(PlayerActor.class);
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CurrentTurnRequest.class,msg->{
                    getSender().tell(currentTurn,getSelf());
                })
                .match(IncreaseTurnRequest.class, msg->{
                    currentTurn+=1;
                    getSender().tell(currentTurn,getSelf());
                }).match(DoTransactionRequest.class,msg->{
                    doTransaction(msg.getUsername());
                })
                .build();
    }

    private void doTransaction(String userName){


        List<Stock> stockList = GameController.stocks;

        Stock mostIncreased = stockList.get(0);
        float mostIncreasedValue = 0f;
        Stock leastIncreased = stockList.get(0);
        float leastIncreasedValue = 0f;

        for (int i = 0;i<stockList.size();i++){
            Stock stock = stockList.get(i);
            List<Float> priceList = stock.getPriceList();
            int size = priceList.size();
            float change;

            if(size > 1){
                change = priceList.get(size-1) - priceList.get(size-2);
            }else{
                change = priceList.get(size-1);
            }

            if(change > mostIncreasedValue){
                mostIncreasedValue = change;
                mostIncreased = stock;
            }

            if(change < leastIncreasedValue){
                leastIncreasedValue = change;
                leastIncreased = stock;
            }
        }

        List<StockData> stockData = BrokerController.getStockData(userName);
        boolean isSell = false;
        int amount = 0;
        for(int j = 0; j < stockData.size() ; j++){
            if(stockData.get(j).getStock().getName().equals(leastIncreased.getName())){
                if(stockData.get(j).getAmount() != 0){
                    isSell = true;
                }
                amount = stockData.get(j).getAmount();
            }
        }
        Transaction transaction = new Transaction();
        transaction.setUsername(userName);

        if(isSell){
            transaction.setStock(leastIncreased);
            transaction.setAmount(amount);
            transaction.setAction("sell");
        }else {
            transaction.setStock(mostIncreased);
            transaction.setAction("buy");
            transaction.setAmount(10);
        }
        System.out.println(transaction.getStock().getName());
        System.out.println(transaction.getAction());
        BrokerController.performTransaction(transaction);
    }
}
