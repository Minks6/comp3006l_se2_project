package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.stock.market.simulator.simulator.Actors.*;
import com.stock.market.simulator.simulator.Messages.CreateBankAccount;
import com.stock.market.simulator.simulator.Messages.DoTransactionRequest;
import com.stock.market.simulator.simulator.Messages.InitializeStocks;

import java.util.HashMap;
import java.util.Map;

public class ActorController {

    static ActorSystem system = ActorSystem.create("sample1");
    static ActorRef bank;
    static ActorRef broker;
    static ActorRef clock;
    static ActorRef market;
    static ActorRef computerPlayer;
    static Map<String,ActorRef> players = new HashMap<>();
    static String  computerPlayerUsername = "computerPlayer";


    public static void initiateSystem(){
        system = ActorSystem.create("stockMarketSimulator");
        bank = system.actorOf(BankActor.props(), "bank");
        broker = system.actorOf(BrokerActor.props(),"broker");
        clock  = system.actorOf(ClockActor.props(),"clock");
        market = system.actorOf(MarketActor.props(),"market");
        registerUser(computerPlayerUsername);
        computerPlayer = players.get(computerPlayerUsername);
        market.tell(new InitializeStocks(),ActorRef.noSender());
        ActorController.bank.tell(new CreateBankAccount(computerPlayerUsername,1000),ActorRef.noSender());

    }

    public static void sendMessageToBank(Object msg,ActorRef sender){
        bank.tell(msg,sender);
    }

    public static boolean registerUser(String userName){
        if(players.get(userName) != null){
            return false;
        }
        ActorRef user = system.actorOf(PlayerActor.props(),userName);
        players.put(userName, user);
        return true;
    }

    public static boolean loginCheck(String username){
        if(players.get(username) != null){
            return true;
        }
        return false;
    }


}
