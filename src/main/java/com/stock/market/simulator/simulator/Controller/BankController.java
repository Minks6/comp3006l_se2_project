package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.BankTransaction;
import com.stock.market.simulator.simulator.Messages.CheckBankAccountBalance;
import com.stock.market.simulator.simulator.Messages.GetAllUsersRequest;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BankController {

    static Timeout timeout = new Timeout(100, TimeUnit.MILLISECONDS);

    public static float checkBankAccountBalance(String bankAccountName){
        Future<Object> future = Patterns.ask(ActorController.bank, new CheckBankAccountBalance(bankAccountName),
                timeout);
        Float accountBalance = 0.0f;
        try{
            accountBalance = (Float) Await.result(future, timeout.duration());
            accountBalance = ((int)(accountBalance * 100) )/ 100.0f;
        }catch (Exception e){
            e.printStackTrace();
        }
        return accountBalance;
    }

    public static List<UserDto> getAllUsers(){
        Future<Object> future = Patterns.ask(ActorController.bank, new GetAllUsersRequest(),
                timeout);
        List<UserDto> userList = new ArrayList<>();
        try{
            userList = (List<UserDto>) Await.result(future, timeout.duration());
        }catch (Exception e){
            e.printStackTrace();
        }
        return userList;
    }

    public static void transaction(BankTransaction transaction){
        ActorController.bank.tell(transaction, ActorRef.noSender());
    }
}
