package com.stock.market.simulator.simulator.Controller;

import akka.pattern.Patterns;
import akka.util.Timeout;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.GetAllUsersRequest;
import com.stock.market.simulator.simulator.Messages.StockRequest;
import com.stock.market.simulator.simulator.Messages.Transaction;
import com.stock.market.simulator.simulator.Models.StockData;
import com.stock.market.simulator.simulator.Models.TransactionStatus;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BrokerController {
    static Timeout timeout = new Timeout(1000, TimeUnit.MILLISECONDS);

    public static boolean checkBankAccount(String username,float amount){
        float currentAmount = BankController.checkBankAccountBalance(username);
        if(currentAmount > amount){
            return true;
        }
        return false;
    }

    public static List<StockData> getStockData(String username){
        Future<Object> future = Patterns.ask(ActorController.broker, new StockRequest(username),
                timeout);
        List<StockData> stockData = new ArrayList<>();
        try{
            stockData = (List<StockData>) Await.result(future, timeout.duration());
        }catch (Exception e){
            e.printStackTrace();
        }
        return stockData;
    }

    public static TransactionStatus performTransaction(Transaction transaction){
        TransactionStatus transactionStatus = new TransactionStatus();

        try{

            if(PlayerController.getTurnNumber(transaction.getUsername()) < ClockController.getTurnNumber().getTurnNo()){
                Future<Object> future = Patterns.ask(ActorController.broker, transaction,
                        timeout);
                transactionStatus = (TransactionStatus) Await.result(future, timeout.duration());
                if(transactionStatus.isStatus()){
                    int turn = PlayerController.increaseTurnCount(transaction.getUsername());
                    transactionStatus.setTurn(turn);
                    PlayerController.updateGlobalTurn();
                }else{
                    int turn = PlayerController.getTurnNumber(transaction.getUsername());
                    transactionStatus.setTurn(turn);
                }
            }else{
                transactionStatus.setMsg("Please wait until other players complete their turn");
                transactionStatus.setStatus(false);
                transactionStatus.setStocks(BrokerController.getStockData(transaction.getUsername()));
                transactionStatus.setTurn(PlayerController.getTurnNumber(transaction.getUsername()));
            }


        }catch (Exception e){
            e.printStackTrace();
        }
        return transactionStatus;
    }


}
