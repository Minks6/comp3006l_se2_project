package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.stock.market.simulator.simulator.Dto.GameStatusDto;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.CurrentTurnRequest;
import com.stock.market.simulator.simulator.Messages.IncreaseTurnRequest;
import com.stock.market.simulator.simulator.Models.StockData;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ClockController {
    static Timeout timeout = new Timeout(100, TimeUnit.MILLISECONDS);

    public static GameStatusDto getTurnNumber(){

        Future<Object> future = Patterns.ask(ActorController.clock, new CurrentTurnRequest(),
                timeout);
        Integer currentTurn = 0;
        GameStatusDto gameStatusDto = new GameStatusDto();

        try{
            currentTurn = (Integer) Await.result(future, timeout.duration());
            if(currentTurn > GameController.maxNoOfTurns){
                gameStatusDto.setGameOver(true);
                currentTurn-=1;
                gameStatusDto.setTurnNo(currentTurn);
                List<UserDto> users = BankController.getAllUsers();
                float maxAmount=0;
                String wonPlayer="";
                for(int i=0 ; i<users.size() ; i++){
                    UserDto userDto = users.get(i);
                    float tempBalance = userDto.getAmount();
                    float stockValue = 0f;
                    List<StockData> stocks = BrokerController.getStockData(userDto.getUsername());
                    for (int j=0;j<stocks.size();j++){
                        stockValue+= stocks.get(j).getStock().getPrice()*stocks.get(j).getAmount();
                    }
                    tempBalance+= stockValue;

                    if(tempBalance > maxAmount){
                        maxAmount = tempBalance;
                        wonPlayer = userDto.getUsername();
                    }
                }
                gameStatusDto.setMsg("Game Over!!\n"+wonPlayer+" won the game ");
            }else{
                gameStatusDto.setGameOver(false);
                gameStatusDto.setMsg("");
                gameStatusDto.setTurnNo(currentTurn);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return gameStatusDto;
    }

    public static int increaseTurnCount(){
        Future<Object> future = Patterns.ask(ActorController.clock, new IncreaseTurnRequest(),
                timeout);
        Integer currentTurn = 0;
        try{
            currentTurn = (Integer) Await.result(future, timeout.duration());
        }catch (Exception e){
            e.printStackTrace();
        }
        return currentTurn;
    }
}
