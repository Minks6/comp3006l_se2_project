package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import com.stock.market.simulator.simulator.Dto.GameStatusDto;
import com.stock.market.simulator.simulator.Dto.StockDto;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.BankTransaction;
import com.stock.market.simulator.simulator.Messages.CreateBankAccount;
import com.stock.market.simulator.simulator.Messages.Transaction;
import com.stock.market.simulator.simulator.Models.Stock;
import com.stock.market.simulator.simulator.Models.StockData;
import com.stock.market.simulator.simulator.Models.TransactionStatus;
import com.sun.corba.se.pept.broker.Broker;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GUIController {

    @GetMapping("/test")
    public void sendMessageToBank(){
        ActorController.sendMessageToBank("test", ActorRef.noSender());
    }

    @GetMapping("/register")
    public boolean registerUser(@RequestParam String username){
        try{
            ActorController.registerUser(username);
            ActorController.bank.tell(new CreateBankAccount(username,1000),ActorRef.noSender());
            return true;
        }catch (Exception e){
            return false;
        }
    }

    @GetMapping("/loginCheck")
    public boolean loginCheck(@RequestParam String username){
            return ActorController.loginCheck(username);
    }

    @GetMapping("/getStockData")
    public List<StockDto> stocksList(){
        List<StockDto> stockDtoList = new ArrayList<>();
        GameController.stocks.forEach(stock -> {
            StockDto stockDto = new StockDto();
            stockDto.setName(stock.getName());
            stockDto.setSector(stock.getSector().getName());
            stockDto.setPrice(stock.getPrice());
            stockDtoList.add(stockDto);
        });

        return stockDtoList;
    }

    @GetMapping("/getUsers")
    public List<UserDto> userList(){
        List<UserDto> users = BankController.getAllUsers();
        users.forEach(userDto -> {
                float stockValue = 0f;
                List<StockData> stocks = BrokerController.getStockData(userDto.getUsername());
                for (int i=0;i<stocks.size();i++){
                    stockValue+= stocks.get(i).getStock().getPrice()*stocks.get(i).getAmount();
                }
                userDto.setAmount(userDto.getAmount()+stockValue);
                userDto.setTurn(PlayerController.getTurnNumber(userDto.getUsername()));

        });

        return users;
    }

    @GetMapping("/getStocks")
    public List<StockData> getStockDetails(@RequestParam String username){
        return BrokerController.getStockData(username);
    }

    @GetMapping("/transaction")
    public TransactionStatus transaction(@RequestParam String name,@RequestParam int amount, @RequestParam String action, @RequestParam String username){
        Transaction transaction = new Transaction();
        transaction.setUsername(username);
        transaction.setAction(action);
        transaction.setAmount(amount);
        GameController.stocks.forEach(stock->{
            if(stock.getName().equals(name)){
                transaction.setStock(stock);
            }
        });

        TransactionStatus response = BrokerController.performTransaction(transaction);
        return response;
    }

    @GetMapping("/getTurn")
    public GameStatusDto getTurn(){
        return ClockController.getTurnNumber();
    }

    @GetMapping("/balance")
    public float getBankBalance(@RequestParam String username){
        float balance = BankController.checkBankAccountBalance(username);
        return balance;
    }
}
