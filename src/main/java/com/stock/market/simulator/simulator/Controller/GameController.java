package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.stock.market.simulator.simulator.Messages.DoTransactionRequest;
import com.stock.market.simulator.simulator.Models.Sector;
import com.stock.market.simulator.simulator.Models.Stock;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class GameController {
    public static List<Sector> sectors = new ArrayList<>();
    public static List<Stock> stocks = new ArrayList<>();
    public static final int maxNoOfTurns = 10;
    static Timeout timeout = new Timeout(100, TimeUnit.MILLISECONDS);

    public static void initializeStocks(){
        Sector financeSector = new Sector("Finance");
        Sector technologySector = new Sector("Technology");
        Sector healthSector = new Sector("Health");
        Sector apparelSector = new Sector("Apparel");

        sectors.add(financeSector);
        sectors.add(technologySector);
        sectors.add(healthSector);
        sectors.add(apparelSector);

        stocks.add(new Stock("HNB Bank",financeSector));
        stocks.add(new Stock("NTB Bank",financeSector));
        stocks.add(new Stock("BOC Bank",financeSector));

        stocks.add(new Stock("WSO2",technologySector));
        stocks.add(new Stock("MIT Technology",technologySector));
        stocks.add(new Stock("Demo Pvt Ltd",technologySector));

        stocks.add(new Stock("Asiri Hospital",healthSector));
        stocks.add(new Stock("Nawaloka Hospital",healthSector));
        stocks.add(new Stock("Lanka Hospital ",healthSector));

        stocks.add(new Stock("MAS Holding",apparelSector));
        stocks.add(new Stock("Brandix Lanka",apparelSector));
        stocks.add(new Stock("MAS Active",apparelSector));

        ActorController.computerPlayer.tell(new DoTransactionRequest(ActorController.computerPlayerUsername), ActorRef.noSender());


    }
}
