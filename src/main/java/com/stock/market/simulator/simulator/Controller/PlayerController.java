package com.stock.market.simulator.simulator.Controller;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import com.stock.market.simulator.simulator.Dto.GameStatusDto;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.*;
import scala.concurrent.Await;
import scala.concurrent.Future;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class PlayerController {
    static Timeout timeout = new Timeout(2000, TimeUnit.MILLISECONDS);

    public static int getTurnNumber(String username){

        ActorRef user = ActorController.players.get(username);
        Future<Object> future = Patterns.ask(user, new CurrentTurnRequest(),
                timeout);
        Integer currentTurn = 0;
        try{
            currentTurn = (Integer) Await.result(future, timeout.duration());
        }catch (Exception e){
//            e.printStackTrace();
        }
        return currentTurn;
    }

    public static int increaseTurnCount(String username){
        ActorRef user = ActorController.players.get(username);
        Future<Object> future = Patterns.ask(user, new IncreaseTurnRequest(),
                timeout);
        Integer currentTurn = 0;
        try{
            currentTurn = (Integer) Await.result(future, timeout.duration());
        }catch (Exception e){
//            e.printStackTrace();
        }
        return currentTurn;
    }

    public static GameStatusDto updateGlobalTurn(){
        List<String> completedUsers = new ArrayList<>();
        ActorController.players.forEach((userName,actor) ->{
            if(PlayerController.getTurnNumber(userName) == ClockController.getTurnNumber().getTurnNo()){
                completedUsers.add(userName);
            }
        });

        if(completedUsers.size() == ActorController.players.size()){
            int turn = ClockController.increaseTurnCount();
            if(turn <= GameController.maxNoOfTurns){
                ActorController.computerPlayer.tell(new DoTransactionRequest(ActorController.computerPlayerUsername),ActorRef.noSender());
                ActorController.market.tell(new UpdateStockPrices(turn),ActorRef.noSender());
            }

        }

        return ClockController.getTurnNumber();
    }
}
