package com.stock.market.simulator.simulator.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {

    @GetMapping("/login")
    public String login(){
        return "login.html";
    }

    @GetMapping("/game")
    public String game(){
        return "game.html";
    }
}
