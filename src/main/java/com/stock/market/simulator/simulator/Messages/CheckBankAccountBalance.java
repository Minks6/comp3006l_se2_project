package com.stock.market.simulator.simulator.Messages;

public class CheckBankAccountBalance {

    public CheckBankAccountBalance(String accountName) {
        this.accountName = accountName;
    }

    private String accountName;

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
