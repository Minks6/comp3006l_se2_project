package com.stock.market.simulator.simulator.Messages;

public class CreateBankAccount {
    private float amount;
    private String accountHolderName;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        this.accountHolderName = accountHolderName;
    }

    public CreateBankAccount(String accountHolderName, float amount) {
        this.amount = amount;
        this.accountHolderName = accountHolderName;
    }
}
