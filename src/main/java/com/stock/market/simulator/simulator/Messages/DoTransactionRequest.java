package com.stock.market.simulator.simulator.Messages;

public class DoTransactionRequest {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public DoTransactionRequest(String username) {
        this.username = username;
    }
}
