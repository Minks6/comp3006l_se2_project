package com.stock.market.simulator.simulator.Messages;

public class StockRequest {
    private String username;

    public StockRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
