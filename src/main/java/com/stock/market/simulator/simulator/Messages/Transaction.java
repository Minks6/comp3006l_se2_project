package com.stock.market.simulator.simulator.Messages;

import com.stock.market.simulator.simulator.Models.Stock;

public class Transaction {
    private String action;
    private String username;
    private Stock stock;
    private int amount;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
