package com.stock.market.simulator.simulator.Messages;

public class UpdateStockPrices {
    private int turn;

    public UpdateStockPrices(int turn) {
        this.turn = turn;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }
}
