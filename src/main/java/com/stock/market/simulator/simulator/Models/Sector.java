package com.stock.market.simulator.simulator.Models;

import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Util.RandomGeneratorUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Sector {

    private String name;
    Random rand = new Random();
    private List<Integer> trends;

    public Sector(String name) {
        this.name = name;
        this.trends = calculatedSectorTrend();
    }

    public String getName() {
        return name;
    }

    public List<Integer> getTrends() {
        return trends;
    }

    public void setTrends(List<Integer> trends) {
        this.trends = trends;
    }

    private List<Integer> calculatedSectorTrend(){
        List<Integer> trends = new ArrayList<>();
        int currentVal = 0;
        for(int i = 0; i< GameController.maxNoOfTurns; i+=1){
            if(i==0){
                currentVal = RandomGeneratorUtil.generateRandom(-3,3);
                trends.add(currentVal);
            }else{
                int probability = rand.nextInt(4);
                if(probability < 2){
                    trends.add(currentVal);
                }else if(probability == 2){
                    trends.add(RandomGeneratorUtil.generateRandom(currentVal,3));
                }else if(probability == 3){
                    trends.add(RandomGeneratorUtil.generateRandom(-3,currentVal));
                }
            }
        }
        return trends;
    }

    public void setName(String name) {
        this.name = name;
    }
}
