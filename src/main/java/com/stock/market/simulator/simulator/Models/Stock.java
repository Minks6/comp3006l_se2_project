package com.stock.market.simulator.simulator.Models;

import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Util.RandomGeneratorUtil;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Stock {
    private String  name;
    private float price;
    private List<Integer> trends;
    private Sector sector;
    private List<Float> priceList;
    private Random rand = new Random();

    public Stock(String name, Sector sector) {
        this.name = name;
        this.price = rand.nextInt(10)+1;
        this.sector = sector;
        this.trends = calculatedMarketTrend();
        this.priceList = new ArrayList<>();
        priceList.add(price);
    }

    public List<Integer> getTrends() {
        return trends;
    }

    public void setTrends(List<Integer> trends) {
        this.trends = trends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Sector getSector() {
        return sector;
    }

    public void setSector(Sector sector) {
        this.sector = sector;
    }

    public List<Float> getPriceList() {
        return priceList;
    }

    public void setPriceList(List<Float> priceList) {
        this.priceList = priceList;
    }


    private int randomMarketComponent(){
        return RandomGeneratorUtil.generateRandom(-2,2);
    }

    private List<Integer> calculatedMarketTrend(){
        List<Integer> trends = new ArrayList<>();
        int currentVal = 0;
        for(int i = 0; i< GameController.maxNoOfTurns;i++){
            if(i==0){
                currentVal = RandomGeneratorUtil.generateRandom(-5,5);
                trends.add(currentVal);
            }else{
                int probability = rand.nextInt(4);
                if(probability < 2){
                    trends.add(currentVal);
                }else if(probability == 2){
                    trends.add(RandomGeneratorUtil.generateRandom(currentVal,5));
                }else if(probability == 3){
                    trends.add(RandomGeneratorUtil.generateRandom(-5,currentVal));
                }
            }
        }
        return trends;
    }

    public void calculateNewPrice(int turn){
        int randomTrend = randomMarketComponent();
        int marketTrend = this.trends.get(turn-1);
        int sectorTrend = this.sector.getTrends().get(turn-1);
        int totalIncrement = randomTrend+marketTrend+sectorTrend;

        totalIncrement = totalIncrement > 0 ? totalIncrement : 0;

        float newPrice = (this.price*(100+totalIncrement))/100;
        this.price = ((int)(newPrice * 100) )/ 100.0f;
        this.priceList.add(this.price);
    }

}
