package com.stock.market.simulator.simulator.Models;

public class StockData {
    private Stock stock;
    private int amount;

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
