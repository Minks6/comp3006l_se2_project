package com.stock.market.simulator.simulator.Models;

import java.util.List;

public class TransactionStatus {
    private boolean status;
    private List<StockData> stocks;
    private String msg;
    private int turn;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<StockData> getStocks() {
        return stocks;
    }

    public void setStocks(List<StockData> stocks) {
        this.stocks = stocks;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }
}
