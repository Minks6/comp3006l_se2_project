package com.stock.market.simulator.simulator;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.stock.market.simulator.simulator.Actors.BankActor;
import com.stock.market.simulator.simulator.Controller.ActorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimulatorApplication {


	public static void main(String[] args) {

		ActorController.initiateSystem();
		SpringApplication.run(SimulatorApplication.class, args);
	}

}
