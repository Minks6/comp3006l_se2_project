package com.stock.market.simulator.simulator.Util;

import java.util.Random;

public class RandomGeneratorUtil {
    static Random random = new Random();
    public static int generateRandom(int low,int high){
        boolean satisfy = false;
        int val = 0;
        int max = Math.max(Math.abs(low),Math.abs(high));
        while (!satisfy){
            int sign = random.nextInt(2);
            val = random.nextInt(max)+1;
            if(sign == 0){
                val = val*-1;
            }
            if(val >= low & val <= high){
                satisfy = true;
            }
        }
        return val;
    }
}
