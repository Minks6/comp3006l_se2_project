function getStockData(option) {
    $.ajax({url: "/getStockData", success: function(result){
        if(result){
            var stockListHtml = '<thead><th>Name</th><th>Sector</th><th>Price(LKR)</th></thead><tbody>';
            var stockOptionList = '';
            console.log(result);
            result.forEach(function (stock) {
                stockListHtml+='<tr>';
                stockListHtml+='<td>'+stock.name+'</td>';
                stockListHtml+='<td>'+stock.sector+'</td>';
                stockListHtml+='<td>'+stock.price+'</td>';
                stockListHtml+='</tr>';

                stockOptionList+='<option value="'+stock.name+'">'+stock.name+'</option>';
            });
            stockListHtml+='</tbody>';
            $("#stockList").html(stockListHtml);
            if(option){
                $("#stockName").html(stockOptionList);
            }
        }else{
            alert("Error occurred while getting stock data")
        }
    }});
}

function getUserList() {
    $.ajax({url: "/getUsers", success: function(result){
        if(result){
            var userListHtml = '<thead><th>Name</th><th>Total (LKR)</th><th>Turn</th></thead>';
            console.log(result);
            result.forEach(function (stock) {
                userListHtml+='<tr>';
                userListHtml+='<td>'+stock.username+'</td>';
                userListHtml+='<td>'+stock.amount+'</td>';
                userListHtml+='<td>'+stock.turn+'</td>';
                userListHtml+='</tr>';
            });
            userListHtml+='</tbody>';
            $("#userList").html(userListHtml);
        }else{
            alert("Error occurred while getting user list")
        }
    }});
}

function getMyStocks(username) {
    $.ajax({url: "/getStocks?username="+username, success: function(result){
        if(result){
            displayMyStocks(result);
        }else{
            alert("Error occurred while getting user stock list")
        }
    }});
}

function displayMyStocks(stocks) {
    var myStockListHtml = '<thead><th>Name</th><th>No.of Stocks</th></thead>';
    stocks.forEach(function (stock) {
        myStockListHtml+='<tr>';
        myStockListHtml+='<td>'+stock.stock.name+'</td>';
        myStockListHtml+='<td>'+stock.amount+'</td>';
        myStockListHtml+='</tr>';
    });
    myStockListHtml+='</tbody>';
    $("#myStocks").html(myStockListHtml);
}

function transaction(username,stock,amount,action) {
    $.ajax({url: "/transaction?name="+stock+'&amount='+amount+'&action='+action+'&username='+username, success: function(result){
        console.log(result);
        if(result.status === false){
            alert(result.msg);
        }else{
            $.cookie('myturn',$.cookie('myturn')+1);
        }
        displayMyStocks(result.stocks);
    }});
}

function loginCheck(username) {
    $.ajax({url: "/loginCheck?username="+username, success: function(result){
        if(!result) {
            window.location.replace("/login");

        }else{
            updateGameScreen(username);
        }
    }});
}

function getTurnNumber() {
    $.ajax({url:"/getTurn",success: function (result) {
        if(result.gameOver){
            alert(result.msg);
            clearInterval(update);
        }else{
            $("#turnNo").html(result.turnNo);
            if($.cookie('turnNo') < result){
                getStockData();
            }
            $.cookie("turnNo",result);
        }


    }})
}

function getBankBalance(username) {
    $.ajax({url:"/balance?username="+username,success: function (result) {
        $("#bankBalance").html(result);
    }})
}




function updateGameScreen(username) {
    getUserList();
    getTurnNumber();
    getStockData(false);
    getBankBalance(username)

}