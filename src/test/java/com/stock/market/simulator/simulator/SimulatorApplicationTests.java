package com.stock.market.simulator.simulator;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.stock.market.simulator.simulator.Actors.BankActor;
import com.stock.market.simulator.simulator.Actors.BrokerActor;
import com.stock.market.simulator.simulator.Actors.ClockActor;
import com.stock.market.simulator.simulator.Actors.PlayerActor;
import com.stock.market.simulator.simulator.Controller.ActorController;
import com.stock.market.simulator.simulator.Controller.GameController;
import com.stock.market.simulator.simulator.Dto.UserDto;
import com.stock.market.simulator.simulator.Messages.*;
import com.stock.market.simulator.simulator.Models.TransactionStatus;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import akka.testkit.javadsl.TestKit;

import java.util.List;

import static org.junit.Assert.assertEquals;


@RunWith(SpringRunner.class)
@SpringBootTest
public class SimulatorApplicationTests {
	static ActorSystem system;

	@Test
	public void contextLoads() {
	}

	@BeforeClass
	public static void setup() {
		system = ActorSystem.create();
	}

	@AfterClass
	public static void teardown() {
		TestKit.shutdownActorSystem(system);
		system = null;
	}

	@Test
	public void testBankActor() {
		final TestKit testProbe = new TestKit(system);
		final ActorRef bankActor = system.actorOf(BankActor.props(), "bank");
		bankActor.tell(new CreateBankAccount("udari",1000),testProbe.getRef());

		bankActor.tell(new CheckBankAccountBalance("udari"),testProbe.getRef());
		Float balance = testProbe.expectMsgClass(Float.class);

		assertEquals(new Float(1000.0), balance);
	}

	@Test
	public void testBankActorAllUsers(){
		final TestKit testProbe = new TestKit(system);
		final ActorRef bankActor = system.actorOf(BankActor.props(), "bankAllUserTest");
		bankActor.tell(new CreateBankAccount("udari",1000),testProbe.getRef());
		bankActor.tell(new CreateBankAccount("eranga",500),testProbe.getRef());

		bankActor.tell(new GetAllUsersRequest(),testProbe.getRef());
		List<UserDto> userList = testProbe.expectMsgClass(List.class);

		userList.forEach(userDto -> {
			if(userDto.getUsername().equals("udari")){
				assertEquals(new Float(1000.0), userDto.getAmount());
			}else if(userDto.getUsername().equals("eranga")){
				assertEquals(new Float(500), userDto.getAmount());
			}
		});

	}

	@Test
	public void testBrokerActor(){
		final TestKit testProbe = new TestKit(system);
		final ActorRef brokerActor = system.actorOf(BrokerActor.props(), "broker");
		Transaction transaction = new Transaction();
		ActorController.initiateSystem();
		GameController.initializeStocks();
		transaction.setStock(GameController.stocks.get(0));
		transaction.setAmount(10);
		transaction.setAction("sell");
		transaction.setUsername("udari");

		brokerActor.tell(transaction,testProbe.getRef());
		TransactionStatus response = testProbe.expectMsgClass(TransactionStatus.class);
		assertEquals(false,response.isStatus());
	}

	@Test
	public void testPlayerActor(){
		final TestKit testProbe = new TestKit(system);
		final ActorRef playerActor = system.actorOf(PlayerActor.props(), "player");
		playerActor.tell(new CurrentTurnRequest(),testProbe.getRef());
		Integer response = testProbe.expectMsgClass(Integer.class);
		assertEquals(new Integer(0),response);

	}

	@Test
	public void testClockActor(){
		final TestKit testProbe = new TestKit(system);
		final ActorRef clockActor = system.actorOf(ClockActor.props(), "clock");
		clockActor.tell(new CurrentTurnRequest(),testProbe.getRef());
		Integer response = testProbe.expectMsgClass(Integer.class);
		assertEquals(new Integer(1),response);

	}




}
